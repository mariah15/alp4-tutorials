# ALP4 Tutorial-2

This branch contains all materials for the second tutorial session.

## Agenda

- Recap
- Discussion about programs and processes
- More on C Pointers
- Makefile
- C Programming exercises
- Q&A

