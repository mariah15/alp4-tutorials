#include "../unity/unity.h"
#include "higher_order_func.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

#define INT_ARRAY_SIZE 100

static int int_arr[INT_ARRAY_SIZE];
static int int_arr_copy[INT_ARRAY_SIZE];

void setUp(void) {
  for (int i = 0; i < INT_ARRAY_SIZE; i++) {
    int_arr[i] = i;
    int_arr_copy[i] = i;
  }
}

void tearDown(void) {}

static void id(void *element) { *(int *)element = *(int *)element; }

static bool tautology(void *element) {
  int *element_ptr = (int *)element;
  return *element_ptr >= 0;
}

static bool always_false(void *element) { return *(int *)element < 0; }

static void double_int(void *element) { *((int *)element) *= 2; }

static void plus_2(void *element) { *((int *)element) += 2; }

static void *sum_int(void *acc, void *element) {
  int *acc_ptr = (int *)acc;
  int *element_ptr = (int *)element;
  *acc_ptr += *element_ptr;
  return acc_ptr;
}

static void *product_int(void *acc, void *element) {
  int *acc_ptr = (int *)acc;
  if (*acc_ptr == 0) {
    return acc_ptr;
  }
  int *element_ptr = (int *)element;
  *acc_ptr *= *element_ptr;
  return acc_ptr;
}

static bool is_prime(void *element) {
  int *element_ptr = (int *)element;

  if (*element_ptr < 2) {
    return false;
  }
  for (int i = 2; i < *element_ptr; i++) {
    if (*element_ptr % i == 0) {
      return false;
    }
  }
  return true;
}

static bool is_even(void *element) {
  int *element_ptr = (int *)element;
  return *element_ptr % 2 == 0;
}

static void test_map_empty_array(void) {
  int *result = map(int_arr, 0, sizeof(int), NULL);
  TEST_ASSERT_NULL(result);
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
}

static void test_filter_empty_array(void) {
  size_t new_size;
  int *result = filter(int_arr, 0, sizeof(int), &new_size, NULL);
  TEST_ASSERT_NULL(result);
  TEST_ASSERT_EQUAL_INT(new_size, 0);
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
}

static void test_reduce_empty_array(void) {
  int inital_value = 0;
  int *result = reduce(int_arr, 0, sizeof(int), &inital_value, NULL);
  TEST_ASSERT_EQUAL_INT(*result, inital_value);
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
}

static void test_map_id_array(void) {
  int *result = map(int_arr, INT_ARRAY_SIZE, sizeof(int), id);
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, result, INT_ARRAY_SIZE);
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
  free(result);
}

static void test_filter_totology_array(void) {
  size_t new_size;
  int *result =
      filter(int_arr, INT_ARRAY_SIZE, sizeof(int), &new_size, tautology);
  TEST_ASSERT_EQUAL_INT(new_size, INT_ARRAY_SIZE);
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, result, INT_ARRAY_SIZE);
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
  free(result);
}

static void test_map_double_array(void) {
  int *result = map(int_arr, INT_ARRAY_SIZE, sizeof(int), double_int);
  for (int i = 0; i < INT_ARRAY_SIZE; i++) {
    TEST_ASSERT_EQUAL_INT(int_arr[i] * 2, result[i]);
  }
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
  free(result);
}

static void test_map_plus_2_array(void) {
  int *result = map(int_arr, INT_ARRAY_SIZE, sizeof(int), plus_2);
  for (int i = 0; i < INT_ARRAY_SIZE; i++) {
    TEST_ASSERT_EQUAL_INT(int_arr[i] + 2, result[i]);
  }
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
  free(result);
}

static void test_reduce_sum_array(void) {
  int inital_value = 0;
  int *result =
      reduce(int_arr, INT_ARRAY_SIZE, sizeof(int), &inital_value, sum_int);
  int expected = 0;
  for (int i = 0; i < INT_ARRAY_SIZE; i++) {
    expected += int_arr[i];
  }
  TEST_ASSERT_EQUAL_INT(expected, *result);
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
}

static void test_product_sum_array(void) {
  int inital_value = 1;
  int *result =
      reduce(int_arr, INT_ARRAY_SIZE, sizeof(int), &inital_value, product_int);
  int expected = 1;
  for (int i = 1; i < INT_ARRAY_SIZE; i++) {
    expected *= int_arr[i];
  }
  TEST_ASSERT_EQUAL_INT(expected, *result);
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
}

static void test_filter_is_prime_array(void) {
  size_t new_size;

  int *result =
      filter(int_arr, INT_ARRAY_SIZE, sizeof(int), &new_size, is_prime);
  int expected[] = {2,  3,  5,  7,  11, 13, 17, 19, 23, 29, 31, 37, 41,
                    43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
  TEST_ASSERT_EQUAL_INT(new_size, sizeof(expected) / sizeof(int));
  TEST_ASSERT_EQUAL_INT_ARRAY(expected, result, sizeof(expected) / sizeof(int));
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
  free(result);
}

static void test_filter_always_false_array(void) {
  size_t new_size;
  int *result =
      filter(int_arr, INT_ARRAY_SIZE, sizeof(int), &new_size, always_false);
  TEST_ASSERT_EQUAL_INT(new_size, 0);
  TEST_ASSERT_NULL(result);
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
}

static void test_filter_is_even_array(void) {
  size_t new_size;
  int *result =
      filter(int_arr, INT_ARRAY_SIZE, sizeof(int), &new_size, is_even);
  int expected[50];
  for (int i = 0; i < 50; i++) {
    expected[i] = i * 2;
  }
  TEST_ASSERT_EQUAL_INT(new_size, 50);
  TEST_ASSERT_EQUAL_INT_ARRAY(expected, result, sizeof(expected) / sizeof(int));
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
  free(result);
}

static void test_is_even_and_sum(void) {
  size_t new_size;
  int *result =
      filter(int_arr, INT_ARRAY_SIZE, sizeof(int), &new_size, is_even);
  int inital_value = 0;
  int *sum = reduce(result, new_size, sizeof(int), &inital_value, sum_int);
  int expected = 0;
  for (int i = 0; i < INT_ARRAY_SIZE; i++) {
    if (int_arr[i] % 2 == 0) {
      expected += int_arr[i];
    }
  }
  TEST_ASSERT_EQUAL_INT(new_size, 50);
  TEST_ASSERT_EQUAL_INT(expected, *sum);
  TEST_ASSERT_EQUAL_INT_ARRAY(int_arr, int_arr_copy, INT_ARRAY_SIZE);
  free(result);
}

int main(void) {
  UnityBegin("test_higher_order_func.c");

  RUN_TEST(test_map_empty_array);
  RUN_TEST(test_filter_empty_array);
  RUN_TEST(test_reduce_empty_array);
  RUN_TEST(test_map_id_array);
  RUN_TEST(test_filter_totology_array);
  RUN_TEST(test_map_double_array);
  RUN_TEST(test_reduce_sum_array);
  RUN_TEST(test_filter_is_prime_array);
  RUN_TEST(test_filter_always_false_array);
  RUN_TEST(test_filter_is_even_array);
  RUN_TEST(test_map_plus_2_array);
  RUN_TEST(test_product_sum_array);
  RUN_TEST(test_is_even_and_sum);

  return UnityEnd();
}
