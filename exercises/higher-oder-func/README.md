# Higher Order Functions

Here is a small exercise that helps you understand function pointers and void pointers in C.

## Instructions

Implement some higher order functions for arrays.

You will implement the following functions:

* `map`: map a given array with a transform function to a new array. 
* `filter`: filter a given array with a predicate function to a new array. 
* `reduce`: "reduce" the array with a given initial accumulator and a reducer function to a final value.

## Tips

TBD

## Testing

The included makefile can be used to create and run the tests using the following commands:

```bash
# run unit tests
make test

# check memory leaks
make memcheck

# clean all compilation files
make clean
```

