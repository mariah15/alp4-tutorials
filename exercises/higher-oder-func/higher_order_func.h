#ifndef HIGHER_ORDER_FUNC_H
#define HIGHER_ORDER_FUNC_H

#include <stddef.h>
#include <stdbool.h>

void* map(void* array, size_t n, size_t size, void (*fn)(void* elem));
void* filter(void* array, size_t n, size_t size, size_t* new_size, bool (*pred)(void* elem));
void* reduce(void* array, size_t n, size_t size, void* initial_value, void* (*reducer)(void* acc, void * elem));

#endif

