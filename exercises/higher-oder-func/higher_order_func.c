#include "higher_order_func.h"
#include <stdlib.h>
#include <string.h>

void *map(void *array, size_t n, size_t size, void (*fn)(void *elem)) {
  // TODO: implement me!
}

void *filter(void *array, size_t n, size_t size, size_t *new_size,
             bool (*pred)(void *elem)) {
  // TODO: implement me!
}

void *reduce(void *array, size_t n, size_t size, void *initial_value,
             void *(*reducer)(void *acc, void *elem)) {
  // TODO: implement me!
}
