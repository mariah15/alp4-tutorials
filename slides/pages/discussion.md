---
title: Discussion
---

# Discussion

Discuss (research if necessary) the following topics with your group members:

### Programs and Processes

- What are the differences between programs and processes?
- What information does a process possess?
- How does a process communicate with another posses?
- How to create processes? What are the differences between different approaches?

<br/>
<v-click>

### Exercise

Implement your own [system() function](https://devdocs.io/c/program/system) using child process, so that you can have a naive version of shell.

You can find the source file in `exercises/process`

</v-click>
