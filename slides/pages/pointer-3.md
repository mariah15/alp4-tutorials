---
title: More on Pointers III
---

## Function Pointers

It's possible to take the address of a function, too.

```c
// Pointer to strcpy-like function
char *(*strcpy_ptr)(char *dst, const char *src); 
// Just like in a regular function declaration, the parameter names are optional
char *(*strcpy_ptr_noparams)(char *, const char *) = strcpy_ptr;

// You can also take the address of a function
strcpy_ptr = &strcpy;
strcpy_ptr = strcpy; // this also works 
```

The type of the pointer to `strcpy` is `char *(*)(char *, const char *)`.

We can even have a function that returns a pointer to functions:

```c
char *(*get_strcpy_ptr(void))(char *dst, const char *src);

# for better readability, we need to use typedef
typedef char *(*strcpy_funcptr)(char *, const char *);

strcpy_funcptr strcpy_ptr = strcpy;
strcpy_funcptr get_strcpy_ptr(void);
```
