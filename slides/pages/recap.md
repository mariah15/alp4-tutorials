---
title: Recap
---

# Recap

### Deterministic Algorithm vs Determined Algorithm

<br />

<div v-click>
<strong>Deterministic:</strong> With the same input, you always get the same output, and changes to the states of the system and their order are always the same

<strong>Determined:</strong> With the same input, you always get the same output. But there is no guarantee, that the changes to the states of the system are also the same.
</div>


<v-click>

### Determined Algorithm

</v-click>


<br />

<v-clicks>

- Can you give an example of determined algorithm?
- How can we guarantee the correctness of a determined algorithm?

</v-clicks>

