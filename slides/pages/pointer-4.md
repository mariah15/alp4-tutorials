---
title: More on Pointers IV
layout: two-cols
---

## How to Read C Declarations

**The golden rule**:

Start at the variable name (or innermost construct if no identifier
is present). Look right without jumping over a right parenthesis; say
what you see. Look left again without jumping over a parenthesis; say
what you see. Jump out a level of parentheses if any. Look right;
say what you see. Look left; say what you see. Continue in this
manner until you say the variable type or return type.

**examples:**

```c
int i; // an int
int *a[3];  // an array of size 3 pointers to int
int (*a)[3]; // an pointer of array of size 3 ints
int (*Object_vtable[])(); // an array of pointers to function returning int
```

::right::

### Exercise I

Discuss with your teammates and read these declarations out loud:

- `char ****q[ 30 ];`
- `char **(**q)[ 30 ];`
- `extern int (x)[];`
- `long (*a[])( char, char );`
- `int *(*(*(*b)())[10])();`
- `char *strprt( char (*)( int ), unsigned char );`
- `int (*const ab[])( unsigned int );`

### Exercise II

Implementing higher order functions in C, see [exercises/higher-order-func](https://git.imp.fu-berlin.de/mactavish96/alp4-tutorials/-/tree/tutorial-2/exercises/higher-oder-func)
