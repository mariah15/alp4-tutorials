---
title: More on Pointers II
---

## Pointers and `const`

The `const` keyword is used a bit differently when pointers are involved. These two declarations are equivalent:

```c
// They are both a pointer to a const integer
// you can not change *ptr_a, but you can change ptr_a
const int *ptr_a;
int const *ptr_a;
```

These two, however, are not equivalent:

```c
// ptr_a is a pointer to a const integer
int const *ptr_a;
// ptr_b is a const pointer to a integer
// changing *ptr_b is valid, but changing ptr_b is invalid
int *const ptr_b;
```
