---
title: More on Pointers I
---

# More on Pointers

## Multiple indirection

Consider the following code:

```c
int    a =  3;
int   *b = &a;
int  **c = &b;
int ***d = &c;
```

Here are how the values of these pointers equate to each other:

```c
  *d ==   c; Dereferencing an (int ***) once gets you an (int **)

 **d ==  *c ==  b; Dereferencing an (int ***) twice, or an (int **) once, gets you an (int *) 

***d == **c == *b == a == 3; Dereferencing an (int ***) thrice, or an (int **) twice, or an (int *) once
```

