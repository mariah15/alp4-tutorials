---
title: Q&A
---

# Q&A

Questions about:

- First Assignment Sheet
- Second Assignment Sheet
- Programming exercises
- Makefile
- Organisation

<br/>

### Materials

- [Reading C Declarations: A Guide for the Mystified](http://www.ericgiguere.com/articles/reading-c-declarations.html)
- [Higher Order Functions in C](https://foxypanda.me/higher-order-functions-in-c/)
